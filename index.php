<?php
session_start();

/**
 * Copyright (c) 2013 Matthew Gall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

/**
* Required includes
**/
require_once('config.php');
require_once('_sdk/facebook.php');

/**
* Instantiate a Facebook object to converse with the Facebook API
**/
$facebook = new Facebook(array(
	'appId' => APPID,
	'appSecret' => APPSECRET));

/**
* Now we have a Facebook object, we can go and talk Facebook, this if() statement will decide if we have a
* token or not.
**/
if ( isset($_GET['code']) or isset($_SESSION['access_token']) ) {
	// They are logged in, so I am setting that as true
	$loggedin = true;

	// First, check if the access_token has been established
	if ( !isset($_SESSION['access_token']) ) {
		// No access_token has been set by me, so we'll move to get their access_token
		$token_url = "https://graph.facebook.com/oauth/access_token?"
					 . "client_id=" . APPID . "&redirect_uri=" . urlencode("http://matthewgall-facebooksample.herokuapp.com/")
    				 . "&client_secret=" . APPSECRET . "&code=" . $_GET['code'];

     	$response = file_get_contents($token_url);
     	$params = null;
     	parse_str($response, $params);

     	// And now we need to store the information I have obtained
     	$_SESSION['access_token'] = $params['access_token'];
     	$_SESSION['expiry'] = $_params['expiry'];

     	// And set $access_token and $expiry to match
     	$access_token = $_SESSION['access_token'];
     	$expiry = $_SESSION['expiry'];
	}
	else {
		// We have it, so we'll just save the information!
		$access_token = $_SESSION['access_token'];
     	$expiry = $_SESSION['expiry'];
	}

	// Now, we are going to get their information
	$user_url = "https://graph.facebook.com/me?access_token=" . $access_token;
	$photo_url = "https://graph.facebook.com/me/picture?type=large&width=320&redirect=false&access_token=" . $access_token;
	$friends_url = "https://graph.facebook.com/me/friends?access_token=" . $access_token;

	$user = json_decode( file_get_contents($user_url) );
	$photo = json_decode( file_get_contents($photo_url) );
	$friend = json_decode( file_get_contents($friends_url) );

}
else {
	$loggedin = false;
}

/**
 * Now we can start the header
 **/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Facebook Bubbles &raquo; facebook-sample-application</title>
	<link href='_css/style.css' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
</head>

<body>
	<div id="fixed_navbar">
		<h1>Make your Facebook bubble... a sample application by Matthew Gall</h1>
	</div>

	<?php
	if ( $loggedin == false ) {
	?>
	<div id="login_container">
		<h2>Login to...</h2>
		<?php $urlParams = array('scope' => 'read_stream, friends_likes, user_about_me, user_hometown, user_likes, user_photos'); ?>
		<a href="<?php echo $facebook->getLoginUrl($urlParams); ?>"><img src="/_img/facebook.png" /></a><a href="#"><img src="/_img/twitter.png" /></a>
	</div>
	<?php
	}
	else{
	?>
	<div id="welcome_container">
		<div class="circle" style="background-image: url('<?php echo $photo->data->url; ?>');">
			<p><?php echo $user->name; ?></p>
		</div>

		<div class="small-circle" style="background-image: url('<?php echo $photo->data->url; ?>'); margin-left: 250px; margin-top: -50px">
			<p>Friend placeholder</p>
		</div>
	</div>
	<?php
	}
	?>

<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://a.mgall.me/" : "http://a.mgall.me/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 8);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://a.mgall.me/piwik.php?idsite=8" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
</body>
</html>